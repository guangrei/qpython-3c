# About

<p align="center"><a href="https://gitee.com/buddygr/qpython-3c/raw/master/qpython/src/main/res/drawable-xxhdpi/ic_launcher.png" target="_blank"><img src="https://gitee.com/buddygr/qpython-3c/raw/master/qpython/src/main/res/drawable-xxhdpi/ic_launcher.png"></a></p>

Welcome to the QPython project!

QPython is the Python engine for android. It contains some amazing features such as Python interpreter, runtime environment, editor and QPYPI and integrated SL4A. It makes it easy for you to use Python on Android. And it's FREE.

Compared with other Python apps, QPython mainly solves how to use Python to drive your Android device work. Good SL4A support is our main goal, such as android's camera, sensor, sms, media, storage, crypto, GUI APIs etc.

QPython already has millions of users worldwide and it is also an open source project.

For different usage scenarios, QPython has three branches, namely [QPython Ox , QPython 3x](https://github.com/qpython-android/qpython) and [QPython 3C](https://gitee.com/buddygr/qpython-3c) .

QPython Ox is mainly aimed at programming learners, and it provides more friendly features for beginners.  
QPython 3x is mainly for experienced Python users, and it provides some advanced technical features.  
QPython 3C is forked from QPython OP (one branch of QPython Ox) . QPython 3C is mainly aimed at Android 7.0 to 13 with 64bit, and it provides a lot of advanced technical features .

## Quick start

This repository is the QPython 3C project repository, you can follow the below steps to run it.

- Macos + Android studio, (Ubuntu may work too)
- git clone https://gitee.com/buddygr/qpython-3c.git
- git submodule init
- git submodule sync
- git submodule update
- build it...

### Learn 学习
- Get more information from [wiki](https://github.com/qpython-android/qpython/wiki) for developing QPython OP .
- Get more information from [开源库 in Bilibili](https://www.bilibili.com/read/readlist/rl321663) for developing QPython 3C .

## Download in Net Disk 网盘下载
- All Content 全面内容：[Baidu Net Disk 百度网盘](https://pan.baidu.com/s/1zT1NGtYTe55m6bSRWlePRg) Code提取码:zxcv
- New Content 最新内容：[Tencent Net Disk 腾讯微云](https://share.weiyun.com/Nz85QWKA)

## Related 相关链接

- [Setup QPython 3C](https://www.bilibili.com/read/cv13322251)
- [QPySL4A APIs](https://github.com/qpython-android/qpysl4a/blob/master/doc/en/APIs.rst) and [test scripts](https://github.com/qpython-android/qpysl4a/issues/1)
- [new SL4A Functions 拍照录音录像、截屏录屏](https://www.bilibili.com/read/cv13418026)
- [new SL4A Functions 外置卡读写](https://www.bilibili.com/read/cv18800240)
- [new SL4A Functions 电话、网页、系统等](https://www.bilibili.com/read/cv11197543)
- [new SL4A Functions 可视化窗口、悬浮窗](https://www.bilibili.com/read/cv16138580)
- [new SL4A Functions Javax.Crypto.Cipher 加解密](https://www.bilibili.com/read/cv11108237)
- new SL4A Functions [Open File](https://www.bilibili.com/read/cv11037013) and [Video Play](https://www.bilibili.com/BV1xK4y1D7eV)
- new SL4A games [贪食蛇](https://www.bilibili.com/video/BV1rR4y1j7HF) and [字母消除](https://www.bilibili.com/video/BV1ge4y1H7AW)
- 图形界面：[音乐播放器、文件选择器、列表选择器](https://www.bilibili.com/BV1Pz4y1C7ds)、[长文本框](https://www.bilibili.com/BV1d7411R7ic)
- 网络监测：[基站和综合网络](https://www.bilibili.com/read/cv16061380)与[WiFi网络](https://www.bilibili.com/read/cv13278468)
- [位置](https://www.bilibili.com/read/cv11339588)与[导航卫星](https://www.bilibili.com/read/cv18956157)
- 传感器：[加速度、磁力计、计步器、光线](https://www.bilibili.com/read/cv16824060)、[电池](https://www.bilibili.com/read/cv17813243)与[分贝计](https://www.bilibili.com/read/cv18422794)

## How to ask QPython related questions
In order to benefit those guys who have the same issue with QPython, we suggest that you should ask the issue which is related with QPython within public techical communities.

### Chinese QPythonista Community

- [在segmentfault提问 - Chinese](https://segmentfault.com/t/qpython)
- [在贴吧讨论 - Chinese](https://tieba.baidu.com/f?ie=utf-8&kw=qpython)

### Report issues

Please tell us your phone's informatioin, android os information, QPython branch, and your code, where did you install it and the detailed stituation you have encountered.

#### original QPython
- [Report an app's issue](https://github.com/qpython-android/qpython/issues)
- [Report an non-app's issue](https://github.com/qpython-android/qpython.org/issues)
- [Request to support a package](https://github.com/qpython-android/qpypi/issues)
#### QPython 3C
- [在gitee反馈](https://gitee.com/buddygr/qpython-3c/issues)
- [在B站相关视频或文章下反馈](https://www.bilibili.com/read/readlist/rl321663)

## Donation

### original QPython
- QPYIO (Wechat Video)
- harford (Wechat)
### QPython 3C
- [乘着船 (Bilibili)](https://space.bilibili.com/9185070)

THANK YOU VERY MUCH FOR DONATION!

## How to contribute
WE NEED YOUR HELP AND CONTRIBUTE, WE WILL BE VERY GRATEFUL IF YOU CAN TELL US YOUR IDEA OR SUGGESTION.

- [在贴吧反馈](https://tieba.baidu.com/f?ie=utf-8&kw=qpython)
### original QPython
- QPYIO (Wechat Video)
### QPython 3C
- [在B站相关视频或文章下反馈](https://www.bilibili.com/read/readlist/rl321663)

THANK YOU.

# Thanks to [original QPython project](https://github.com/qpython-android) very much !!!