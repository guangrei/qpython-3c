/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package org.qpython.qsl4a.qsl4a.facade;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;

import com.google.common.collect.Maps;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.qpython.qsl4a.qsl4a.jsonrpc.RpcReceiver;
import org.qpython.qsl4a.qsl4a.rpc.Rpc;
import org.qpython.qsl4a.qsl4a.rpc.RpcDefault;
import org.qpython.qsl4a.qsl4a.rpc.RpcParameter;
import org.qpython.qsl4a.qsl4a.rpc.RpcStartEvent;
import org.qpython.qsl4a.qsl4a.rpc.RpcStopEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This facade exposes the LocationManager related functionality.<br>
 * <br>
 * <b>Overview</b><br>
 * Once activated by 'startLocating' the LocationFacade attempts to return location data collected
 * via GPS or the cell network. If neither are available the last known location may be retrieved.
 * If both are available the format of the returned data is:<br>
 * {u'network': {u'altitude': 0, u'provider': u'network', u'longitude': -0.38509020000000002,
 * u'time': 1297079691231L, u'latitude': 52.410557300000001, u'speed': 0, u'accuracy': 75}, u'gps':
 * {u'altitude': 51, u'provider': u'gps', u'longitude': -0.38537094593048096, u'time':
 * 1297079709000L, u'latitude': 52.41076922416687, u'speed': 0, u'accuracy': 24}}<br>
 * If neither are available {} is returned. <br>
 * Example (python):<br>
 *
 * <pre>
 * import android, time
 * droid = android.Android()
 * droid.startLocating()
 * time.sleep(15)
 * loc = droid.readLocation().result
 * if loc = {}:
 *   loc = getLastKnownLocation().result
 * if loc != {}:
 *   try:
 *     n = loc['gps']
 *   except KeyError:
 *     n = loc['network'] 
 *   la = n['latitude'] 
 *   lo = n['longitude']
 *   address = droid.geocode(la, lo).result
 * droid.stopLocating()
 * </pre>
 *
 * The address format is:<br>
 * [{u'thoroughfare': u'Some Street', u'locality': u'Some Town', u'sub_admin_area': u'Some Borough',
 * u'admin_area': u'Some City', u'feature_name': u'House Numbers', u'country_code': u'GB',
 * u'country_name': u'United Kingdom', u'postal_code': u'ST1 1'}]
 *
 * @author Damon Kohler (damonkohler@gmail.com)
 * @author Felix Arends (felix.arends@gmail.com)
 */
public class LocationFacade extends RpcReceiver {
  private final EventFacade mEventFacade;
  private final Service mService;
  private final Map<String, Location> mLocationUpdates;
  private LocaCallback mLocaCallback;
  private JSONArray mGnssStatus;
  private final LocationManager mLocationManager;
  private final Geocoder mGeocoder;

  private final static String[] GnssTypes = new String[]{
          "UNKNOWN","GPS","SBAS","GLONASS","QZSS","BEIDOU","GALILEO","IRNSS"
  };

  private final LocationListener mLocationListener = new LocationListener() {
    @Override
    public synchronized void onLocationChanged(Location location) {
      mLocationUpdates.put(location.getProvider(), location);
      Map<String, Location> copy = Maps.newHashMap();
      for (Entry<String, Location> entry : mLocationUpdates.entrySet()) {
        copy.put(entry.getKey(), entry.getValue());
      }
      mEventFacade.postEvent("location", copy);
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
  };

  public LocationFacade(FacadeManager manager) {
    super(manager);
    mService = manager.getService();
    mEventFacade = manager.getReceiver(EventFacade.class);
    mGeocoder = new Geocoder(mService);
    mLocationManager = (LocationManager) mService.getSystemService(Context.LOCATION_SERVICE);
    mLocationUpdates = new HashMap<>();
  }

  @Override
  public void shutdown() {
    stopLocating();
  }

  private void check_Access_Fine_Location_Permission() throws Exception {
    if (ActivityCompat.checkSelfPermission(mService, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      throw new Exception(Manifest.permission.ACCESS_FINE_LOCATION);
    }
  }

  @Rpc(description = "Returns availables providers on the phone")
  public List<String> locationProviders() {
    return mLocationManager.getAllProviders();
  }

  @Rpc(description = "Ask if provider is enabled")
  public boolean locationProviderEnabled(
      @RpcParameter(name = "provider", description = "Name of location provider") String provider) {
    return mLocationManager.isProviderEnabled(provider);
  }

  @Rpc(description = "Starts collecting location data.")
  @RpcStartEvent("location")
  public void startLocating(
      @RpcParameter(name = "minUpdateTime", description = "minimum time between updates in milliseconds") @RpcDefault("60000") Integer minUpdateTime,
      @RpcParameter(name = "minUpdateDistance", description = "minimum distance between updates in meters") @RpcDefault("30") Integer minUpdateDistance,
      @RpcParameter(name = "updateGnssStatus", description = "get Global Navigation Satellite System status") @RpcDefault("false") Boolean updateGnssStatus) throws Exception {
    check_Access_Fine_Location_Permission();
    for (String provider : mLocationManager.getAllProviders()) {
      mLocationManager.requestLocationUpdates(provider, minUpdateTime, minUpdateDistance,
          mLocationListener, mService.getMainLooper());
    }
    if(Build.VERSION.SDK_INT >= 28 && updateGnssStatus){
        mLocaCallback = new LocaCallback();
        mLocationManager.registerGnssStatusCallback(mLocaCallback,
                Handler.createAsync(mService.getMainLooper()));
  }}

  @Rpc(description = "Returns the current location as indicated by all available providers.", returns = "A map of location information by provider.")
  public Map<String, JSONObject> readLocation() throws JSONException {
    Map<String,JSONObject> result = new HashMap<>();
    for(String i: mLocationUpdates.keySet()){
      Location l = mLocationUpdates.get(i);
      if(l==null)continue;
      result.put(i,buildJsonLocation(l));
    }
    return result;
  }

  @Rpc(description = "read Global Navigation Satellite System status if Android >= 9 .")
  public JSONArray readGnssStatus(){
    return mGnssStatus;
  }

  @Rpc(description = "Stops collecting location data.")
  @RpcStopEvent("location")
  public synchronized void stopLocating() {
    mLocationManager.removeUpdates(mLocationListener);
    mLocationUpdates.clear();
    if(Build.VERSION.SDK_INT >=28 && mLocaCallback != null){
      mLocationManager.unregisterGnssStatusCallback(mLocaCallback);
      mGnssStatus = null;
    }
  }

  @Rpc(description = "Returns the last known location of the device.", returns = "A map of location information by provider.")
  public Map<String, JSONObject> getLastKnownLocation() throws Exception {
    check_Access_Fine_Location_Permission();
    Map<String, JSONObject> location = new HashMap<>();
    for (String provider : mLocationManager.getAllProviders()) {
      location.put(provider, buildJsonLocation(mLocationManager.getLastKnownLocation(provider)));
    }
    return location;
  }

  @Rpc(description = "Returns a list of addresses for the given latitude and longitude.", returns = "A list of addresses.")
  public JSONObject[] geocode(
      @RpcParameter(name = "latitude") Double latitude,
      @RpcParameter(name = "longitude") Double longitude,
      @RpcParameter(name = "maxResults", description = "maximum number of results") @RpcDefault("1") Integer maxResults)
          throws Exception {
    List<Address> addressList =  mGeocoder.getFromLocation(latitude, longitude, maxResults);
    JSONObject[] address = new JSONObject[addressList.size()];
    for(int i=0;i<addressList.size();i++)
      address[i]=buildJsonAddress(addressList.get(i));
    return address;
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  public class LocaCallback extends GnssStatus.Callback {
    @Override
    public void onSatelliteStatusChanged(GnssStatus status) {
      super.onSatelliteStatusChanged(status);
      //解析组装卫星信息
      try {
        mGnssStatus = buildJsonGnssStatus(status);
      } catch (JSONException e) {
        mGnssStatus = new JSONArray();
        mGnssStatus.put(e.toString());
      }
    }

    @Override
    public void onStarted() {
      super.onStarted();
    }

    @Override
    public void onStopped() {
      super.onStopped();
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.O)
  private static JSONArray buildJsonGnssStatus(GnssStatus status) throws JSONException {
    JSONArray results = new JSONArray();
    int satelliteCount = status.getSatelliteCount();
    for (int i = 0; i < satelliteCount; i++) {
      JSONObject result = new JSONObject();
      result.put("ConstellationType", GnssTypes[status.getConstellationType(i)]);
      result.put("AzimuthDegrees",status.getAzimuthDegrees(i));
      result.put("ElevationDegrees",status.getElevationDegrees(i));
      result.put("Cn0DbHz",status.getCn0DbHz(i));
      result.put("Svid",status.getSvid(i));
      result.put("UsedInFix",status.usedInFix(i));
      results.put(result);
    }
    return results;
  }

  private static JSONObject buildJsonAddress(Address address) throws JSONException {
    JSONObject result = new JSONObject();
    result.put("admin_area", address.getAdminArea());
    result.put("country_code", address.getCountryCode());
    result.put("country_name", address.getCountryName());
    result.put("feature_name", address.getFeatureName());
    result.put("phone", address.getPhone());
    result.put("locality", address.getLocality());
    result.put("postal_code", address.getPostalCode());
    result.put("sub_admin_area", address.getSubAdminArea());
    result.put("thoroughfare", address.getThoroughfare());
    result.put("url", address.getUrl());
    return result;
  }

  private static Map<String,JSONObject> buildJsonLocations(Map<String, Location> source) throws JSONException {
    Map<String,JSONObject> result = new HashMap<>();
    for(String i: source.keySet()){
      Location l = source.get(i);
      if(l==null)continue;
      result.put(i,buildJsonLocation(l));
    }
    return result;
  }

  private static JSONObject buildJsonLocation(Location location) throws JSONException {
    JSONObject result = new JSONObject();
    result.put("altitude", location.getAltitude());
    result.put("latitude", location.getLatitude());
    result.put("longitude", location.getLongitude());
    result.put("time", location.getTime());
    result.put("accuracy", location.getAccuracy());
    result.put("speed", location.getSpeed());
    result.put("provider", location.getProvider());
    result.put("bearing", location.getBearing());
    return result;
  }
}
